﻿function TestCompilerGeneratedStuff__TestEventCheck(this_, del) {
  if (TestCompilerGeneratedStuff__get_IntProperty(this_) == 1)
    TestCompilerGeneratedStuff__remove_testEvent(this_, del);
  else
    TestCompilerGeneratedStuff__add_testEvent(this_, del);
}