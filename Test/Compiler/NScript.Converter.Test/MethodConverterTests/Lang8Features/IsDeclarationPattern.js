﻿function Lang8Features__IsDeclarationPattern() {
  var b, isZero, sb, isSubClass;
  b = SubClass_factory();
  isZero = ((sb = b) || true) && sb.get_x() == 0;
  isSubClass = SubClass.isInstanceOfType(b);
}
