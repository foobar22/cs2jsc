﻿function BasicStatements__TestComplexCondition(i1, i2) {
  return i2 == 3 && i1 == 10 && (i1 == 1 || i2 == 9) && (i1 == 2 || i2 == 3) || i1 == 4 && i2 == 3 ? 10 : 14;
}
