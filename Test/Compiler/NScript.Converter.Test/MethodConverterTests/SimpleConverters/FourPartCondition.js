﻿function BasicStatements__TestFourPartCondition(i) {
  return (i >= 10 || BasicStatements__TestSimpleAndComparision(i)) && i <= 14 && !BasicStatements__TestSimpleCompare(i);
}
