﻿function SwitchTest__RegressSwitchWithFor(action, changeIndex, newItems, oldItems) {
  var index;
  if (this.newItems == newItems)
    return;
  switch(action) {
    case 0: {
      this.newItems.set_item(changeIndex, newItems.get_item(changeIndex));
      break;
    }
    case 1: {
      Class1__GetMoreStatic(action);
      this.simpleIntSwitch(oldItems.gl());
      break;
    }
    case 2: {
      for (index = 0; index < newItems.gl(); ++index) {
        if (!!this.oldItems)
          this.oldItems.set_item(changeIndex + index, newItems.get_item(index));
        this.newItems.set_item(changeIndex + index, newItems.get_item(index));
      }
      break;
    }
    case 4: {
      Class1__GetMoreStatic(this.newItems.gl());
      for (index = 0; index < this.newItems.gl(); ++index)
        this.oldItems.set_item(index, newItems.get_item(index));
      break;
    }
  }
}
