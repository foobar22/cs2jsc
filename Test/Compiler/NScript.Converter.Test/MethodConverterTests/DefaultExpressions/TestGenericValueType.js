﻿function TestDefaultExpr__TestGenericValueType() {
  var s;
  s = {
    x: {
      x: {
        x: 0,
        y: 0,
        z: {
          item1: 0,
          item2: {
            x: null,
            y: 0,
            z: 0
          }
        }
      },
      y: 0,
      z: null
    },
    y: 0,
    z: null
  };
}