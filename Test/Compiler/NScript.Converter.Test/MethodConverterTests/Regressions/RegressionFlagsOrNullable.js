﻿function BasicStatements__RegressionFlagsOrWithNullable(en, other) {
  return en === null || other === null ? null : en | other;
}
