﻿function FuncRegressions__ArrayIndexOfName_a(strArray_a, isNeg_b) {
  return isNeg_b && strArray_a.gl() > 0 ? strArray_a.iof("-1") : -1;
}