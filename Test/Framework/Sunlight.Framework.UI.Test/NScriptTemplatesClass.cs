﻿//-----------------------------------------------------------------------
// <copyright file="NScriptClass1.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.UI.Test
{
    using Sunlight.Framework.UI.Attributes;
    using Sunlight.Framework.UI.Helpers;
    using System;

    /// <summary>
    /// Definition for NScriptClass1
    /// </summary>
    public class NScriptsTemplatesClass
    {
        [Skin("Sunlight.Framework.UI.Test.Templates.TestTemplate1.html")]
        public static Skin TestTemplate1
        {
            get
            { return null; }
        }

        [Skin("Sunlight.Framework.UI.Test.Templates.TestTemplateVMB1.html")]
        public static Skin TestTemplateVMB1
        {
            get
            { return null; }
        }

        [Skin("Sunlight.Framework.UI.Test.Templates.TestTemplateVMB1_int.html")]
        public static Skin TestTemplateVMB1_int
        {
            get
            { return null; }
        }

        [Skin("Sunlight.Framework.UI.Test.Templates.TestTemplateVMC1.html")]
        public static Skin TestTemplateVMC1
        {
            get
            { return null; }
        }

        [Skin("Sunlight.Framework.UI.Test.Templates.TestTemplateVMB_AttrBinding.html")]
        public static Skin TestTemplateVMB_AttrBinding
        {
            get
            { return null; }
        }

        [Skin("Sunlight.Framework.UI.Test.Templates.TestTemplateVMB_CssBinding.html")]
        public static Skin TestTemplateVMB_CssBinding
        {
            get
            { return null; }
        }

        [Skin("Sunlight.Framework.UI.Test.Templates.TestTemplateVMB_StyleBinding.html")]
        public static Skin TestTemplateVMB_StyleBinding
        {
            get
            { return null; }
        }

        [Skin("Sunlight.Framework.UI.Test.Templates.TestTemplateB_PropertyBinding.html")]
        public static Skin TestTemplateB_PropertyBinding
        {
            get
            { return null; }
        }
    }
}
