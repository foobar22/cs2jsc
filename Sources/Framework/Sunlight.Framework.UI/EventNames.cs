﻿//-----------------------------------------------------------------------
// <copyright file="EventNames.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.UI
{
    using System;

    /// <summary>
    /// Definition for EventNames
    /// </summary>
    public static class EventNames
    {
        public const string Click = "click";
        public const string MouseUp = "mouseup";
        public const string MouseDown = "mousedown";
        public const string MouseOver = "mouseover";
        public const string MouseOut = "mosueout";
        public const string KeyDown = "keydown";
        public const string KeyUp = "keyup";
        public const string KeyPress = "keypress";

        public const string TouchStart = "touchstart";
        public const string TouchEnd = "touchend";
        public const string TouchMove = "touchmove";

        public const string Change = "change";
        public const string Focus = "focus";
        public const string Blur = "blur";
        public const string Load = "load";
        public const string Unload = "unload";
        public const string Abort = "abort";
    }
}
