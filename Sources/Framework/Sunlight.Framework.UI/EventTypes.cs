﻿//-----------------------------------------------------------------------
// <copyright file="EventTypes.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.UI
{
    using System;

    /// <summary>
    /// Definition for EventTypes
    /// </summary>
    public enum EventTypes
    {
        Blur,
        Change,
        Focus,
        Reset,
        Select,
        Submit,
        Abort,
        KeyDown,
        KeyUp,
        KeyPress,
        Click,
        DoubleClick,
        MouseDown,
        MouseMove,
        MouseOut,
        MouseOver,
        MouseUp,
    }
}
