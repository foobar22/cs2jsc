﻿//-----------------------------------------------------------------------
// <copyright file="SkinAttribute.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.UI.Attributes
{
    using System;

    /// <summary>
    /// Definition for SkinAttribute
    /// </summary>
    public class SkinAttribute : Attribute
    {
        public SkinAttribute(string skinResourceName)
        {
        }
    }
}
