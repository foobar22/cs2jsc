﻿//-----------------------------------------------------------------------
// <copyright file="CssNameAttribute.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.UI.Attributes
{
    using System;

    /// <summary>
    /// Definition for CssNameAttribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CssNameAttribute : Attribute
    {
    }
}
