namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal enum IDispatchMethodIndices {
        IUnknown_QueryInterface = 0,
        IUnknown_AddRef = 1,
        IUnknown_Release = 2,
        IDispatch_GetTypeInfoCount = 3,
        IDispatch_GetTypeInfo = 4,
        IDispatch_GetIDsOfNames = 5,
        IDispatch_Invoke = 6,
    }
}
