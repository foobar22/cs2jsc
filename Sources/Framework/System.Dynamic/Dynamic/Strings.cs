namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal static class Strings : System.Object {
        extern internal static System.String UnexpectedVarEnum(System.Object p0);
        extern internal static System.String DispBadParamCount(System.Object p0);
        extern internal static System.String DispMemberNotFound(System.Object p0);
        extern internal static System.String DispNoNamedArgs(System.Object p0);
        extern internal static System.String DispOverflow(System.Object p0);
        extern internal static System.String DispTypeMismatch(System.Object p0, System.Object p1);
        extern internal static System.String DispParamNotOptional(System.Object p0);
        extern internal static System.String GetIDsOfNamesInvalid(System.Object p0);
        extern internal static System.String CouldNotGetDispId(System.Object p0, System.Object p1);
        extern internal static System.String AmbiguousConversion(System.Object p0, System.Object p1);
        extern internal static System.String VariantGetAccessorNYI(System.Object p0);
    }
}
