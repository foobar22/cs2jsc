namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal static class Helpers : System.Object {
        extern internal static System.Linq.Expressions.Expression Convert(System.Linq.Expressions.Expression expression, System.Type type);
    }
}
