namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal static class NativeMethods : System.Object {
        extern internal static void  VariantClear(System.IntPtr variant);
    }
}
