namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal struct VariantArray4 {
        public System.Dynamic.Variant Element0;
        public System.Dynamic.Variant Element1;
        public System.Dynamic.Variant Element2;
        public System.Dynamic.Variant Element3;
    }
}
