namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal class ComEventDesc : System.Object {
        internal System.Guid sourceIID;
        internal System.Int32 dispid;
        extern public ComEventDesc();
    }
}
