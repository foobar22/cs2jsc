namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal struct ExcepInfo {
        extern internal void  Dummy();
        extern internal System.Exception GetException();
    }
}
