namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal struct VariantArray2 {
        public System.Dynamic.Variant Element0;
        public System.Dynamic.Variant Element1;
    }
}
