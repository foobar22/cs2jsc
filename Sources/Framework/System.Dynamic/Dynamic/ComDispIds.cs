namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal static class ComDispIds : System.Object {
        internal const System.Int32 DISPID_VALUE = 0;
        internal const System.Int32 DISPID_PROPERTYPUT = -3;
        internal const System.Int32 DISPID_NEWENUM = -4;
    }
}
