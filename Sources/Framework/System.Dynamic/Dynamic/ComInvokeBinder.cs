namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal sealed class ComInvokeBinder : System.Object {
        extern internal System.Dynamic.DynamicMetaObject Invoke();
    }
}
