namespace System.Dynamic
{
    [Obsolete, System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal sealed class SRDescriptionAttribute : System.ComponentModel.DescriptionAttribute {
        extern public SRDescriptionAttribute(System.String description);
    }
}
