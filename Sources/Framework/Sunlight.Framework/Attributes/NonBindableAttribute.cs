﻿//-----------------------------------------------------------------------
// <copyright file="NonBindableAttribute.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.Attributes
{
    using System;

    /// <summary>
    /// Definition for NonBindableAttribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NonBindableAttribute : Attribute
    {
    }
}
