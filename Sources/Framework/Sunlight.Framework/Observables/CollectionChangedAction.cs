﻿//-----------------------------------------------------------------------
// <copyright file="CollectionChangedAction.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.Observables
{
    /// <summary>
    /// Definition for CollectionChangedAction
    /// </summary>
    public enum CollectionChangedAction
    {
        Add = 0,
        Remove = 1,
        Replace = 2,
        Reset = 4
    }
}
