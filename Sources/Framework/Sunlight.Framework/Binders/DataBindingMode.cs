﻿//-----------------------------------------------------------------------
// <copyright file="DataBindingMode.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.Binders
{
    using System;

    /// <summary>
    /// Definition for DataBindingMode
    /// </summary>
    public enum DataBindingMode
    {
        OneTime,
        OneWay,
        TwoWay,
    }
}
