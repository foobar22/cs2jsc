﻿//-----------------------------------------------------------------------
// <copyright file="ISourceTargetBinder.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.Binders
{
    using System;

    /// <summary>
    /// Definition for ISourceTargetBinder
    /// </summary>
    public interface ITargetDataBinder
    {
        void TargetValueUpdated();
    }
}
