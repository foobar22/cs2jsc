﻿//-----------------------------------------------------------------------
// <copyright file="ISourceDataBinder.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sunlight.Framework.Binders
{
    using System;

    /// <summary>
    /// Definition for ISourceDataBinder
    /// </summary>
    public interface ISourceDataBinder
    {
        void SourceValueUpdated();
    }
}
