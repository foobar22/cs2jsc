﻿namespace System
{
    using System.Runtime.CompilerServices;

    [Extended, ScriptNamespace("ss")]
    public class EventArgs
    {
        [PreserveCase]
        public static readonly EventArgs Empty;
    }
}

