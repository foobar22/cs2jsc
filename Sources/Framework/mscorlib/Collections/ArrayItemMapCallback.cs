﻿namespace System.Collections
{
    using System;
    using System.Runtime.CompilerServices;

    [IgnoreNamespace, Extended]
    public delegate object ArrayItemMapCallback(object value);
}

