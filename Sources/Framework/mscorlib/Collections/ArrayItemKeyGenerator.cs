﻿namespace System.Collections
{
    using System;
    using System.Runtime.CompilerServices;

    [Extended, IgnoreNamespace]
    public delegate string ArrayItemKeyGenerator(object value, int index);
}

