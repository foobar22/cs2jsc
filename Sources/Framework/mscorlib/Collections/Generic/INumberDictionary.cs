﻿//-----------------------------------------------------------------------
// <copyright file="INumberDictionary.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Collections.Generic
{
    /// <summary>
    /// Definition for INumberDictionary
    /// </summary>
    public interface INumberDictionary<TValue> : IDictionary<Number, TValue>
    {
    }
}