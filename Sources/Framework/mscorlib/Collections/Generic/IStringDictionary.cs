﻿//-----------------------------------------------------------------------
// <copyright file="IStringDictionary.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Collections.Generic
{
    /// <summary>
    /// Definition for IStringDictionary
    /// </summary>
    public interface IStringDictionary<TValue> : IDictionary<string, TValue>
    {
    }
}