﻿namespace System.Collections
{
    using System;
    using System.Runtime.CompilerServices;

    [Extended, IgnoreNamespace]
    public delegate bool ArrayFilterCallback(object value, int index, Array array);
}

