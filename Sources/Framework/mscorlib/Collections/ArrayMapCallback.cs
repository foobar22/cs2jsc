﻿namespace System.Collections
{
    using System;
    using System.Runtime.CompilerServices;

    [IgnoreNamespace, Extended]
    public delegate object ArrayMapCallback(object value, int index, Array array);
}

