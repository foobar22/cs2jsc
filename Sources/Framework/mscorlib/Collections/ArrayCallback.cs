﻿namespace System.Collections
{
    using System;
    using System.Runtime.CompilerServices;

    [Extended, IgnoreNamespace]
    public delegate void ArrayCallback(object value, int index, Array array);
}

