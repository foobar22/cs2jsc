﻿namespace System.Collections
{
    using System;
    using System.Runtime.CompilerServices;

    [Extended, IgnoreNamespace]
    public delegate bool ArrayItemFilterCallback(object value);
}

