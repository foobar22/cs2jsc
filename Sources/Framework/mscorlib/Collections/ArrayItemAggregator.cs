﻿namespace System.Collections
{
    using System;
    using System.Runtime.CompilerServices;

    [IgnoreNamespace, Extended]
    public delegate object ArrayItemAggregator(object aggregatedValue, object value);
}

