﻿namespace System.Collections
{
    using System;
    using System.Runtime.CompilerServices;

    [IgnoreNamespace, Extended]
    public delegate void ArrayItemCallback(object value);
}

