﻿namespace System
{
    using System.Runtime.CompilerServices;

    [IgnoreNamespace, Extended]
    public delegate void Callback();
}

