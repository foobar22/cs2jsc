﻿namespace System.Xml
{
    using System;
    using System.Runtime.CompilerServices;

    [IgnoreNamespace, Extended]
    public sealed class XmlText : XmlNode
    {
        internal XmlText()
        {
        }

        public XmlText SplitText(int offset)
        {
            return null;
        }

        [IntrinsicProperty]
        public string Data
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        [IntrinsicProperty]
        public int Length
        {
            get
            {
                return 0;
            }
        }
    }
}

