﻿namespace System
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [NonScriptable, Extended, EditorBrowsable(EditorBrowsableState.Never)]
    public struct RuntimeTypeHandle
    {
    }
}

