﻿namespace System
{
    using System.Runtime.CompilerServices;

    [IgnoreNamespace, Extended]
    public sealed class NumberFormatInfo
    {
        private NumberFormatInfo()
        {
        }

        [IntrinsicProperty]
        public int CurrencyDecimalDigits
        {
            get
            {
                return 0;
            }
        }

        [IntrinsicProperty]
        public string CurrencyDecimalSeparator
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string CurrencyGroupSeparator
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public int[] CurrencyGroupSizes
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string CurrencyNegativePattern
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string CurrencyPositivePattern
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string CurrencySymbol
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string NaNSymbol
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string NegativeInfinityText
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string NegativeSign
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public int NumberDecimalDigits
        {
            get
            {
                return 0;
            }
        }

        [IntrinsicProperty]
        public string NumberDecimalSeparator
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string NumberGroupSeparator
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public int[] NumberGroupSizes
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public int PercentDecimalDigits
        {
            get
            {
                return 0;
            }
        }

        [IntrinsicProperty]
        public string PercentDecimalSeparator
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string PercentGroupSeparator
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public int[] PercentGroupSizes
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string PercentNegativePattern
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string PercentPositivePattern
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string PercentSymbol
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string PositiveInfinityText
        {
            get
            {
                return null;
            }
        }

        [IntrinsicProperty]
        public string PositiveSign
        {
            get
            {
                return null;
            }
        }
    }
}

