﻿//-----------------------------------------------------------------------
// <copyright file="InAttribute.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Runtime.InteropServices
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Definition for InAttribute
    /// </summary>
    [Extended, EditorBrowsable(EditorBrowsableState.Never), NonScriptable]
    public class InAttribute : Attribute
    {
    }
}
