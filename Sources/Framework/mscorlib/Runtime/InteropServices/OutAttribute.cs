﻿namespace System.Runtime.InteropServices
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Extended, EditorBrowsable(EditorBrowsableState.Never), NonScriptable]
    public class OutAttribute : Attribute
    {
    }
}

