﻿namespace System.Runtime.CompilerServices
{
    using System;

    [NonScriptable, AttributeUsage(AttributeTargets.Enum, Inherited=false, AllowMultiple=false), Extended]
    public sealed class NamedValuesAttribute : Attribute
    {
    }
}

