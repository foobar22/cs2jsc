﻿//-----------------------------------------------------------------------
// <copyright file="IgnoreGenericArguments.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Runtime.CompilerServices
{
    /// <summary>
    /// Definition for IgnoreGenericArguments
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Type)]
    public sealed class IgnoreGenericArgumentsAttribute : Attribute
    {
    }
}