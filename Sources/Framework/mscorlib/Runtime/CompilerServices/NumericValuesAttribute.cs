﻿namespace System.Runtime.CompilerServices
{
    using System;

    [AttributeUsage(AttributeTargets.Enum, Inherited=false, AllowMultiple=false), Extended, NonScriptable]
    public sealed class NumericValuesAttribute : Attribute
    {
    }
}

