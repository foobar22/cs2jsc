﻿namespace System.Runtime.CompilerServices
{
    using System;
    using System.ComponentModel;

    [Extended, EditorBrowsable(EditorBrowsableState.Never), NonScriptable, AttributeUsage(AttributeTargets.All, Inherited=true)]
    public sealed class CompilerGeneratedAttribute : Attribute
    {
    }
}

