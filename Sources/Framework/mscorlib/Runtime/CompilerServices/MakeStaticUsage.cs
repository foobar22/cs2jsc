﻿//-----------------------------------------------------------------------
// <copyright file="MakeStaticUsage.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Runtime.CompilerServices
{
    /// <summary>
    /// Definition for MakeStaticUsage
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
    public class MakeStaticUsageAttribute : Attribute
    {
    }
}
