﻿namespace System.Runtime.CompilerServices
{
    using System;

    [AttributeUsage(AttributeTargets.Type)]
    public class ExtendedAttribute : Attribute
    {
    }
}

