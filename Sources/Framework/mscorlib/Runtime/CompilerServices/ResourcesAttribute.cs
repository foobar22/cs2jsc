﻿namespace System.Runtime.CompilerServices
{
    using System;

    [AttributeUsage(AttributeTargets.Class), Extended, NonScriptable]
    public sealed class ResourcesAttribute : Attribute
    {
    }
}

