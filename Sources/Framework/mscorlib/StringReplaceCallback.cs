﻿namespace System
{
    using System.Runtime.CompilerServices;

    [Extended, IgnoreNamespace]
    public delegate string StringReplaceCallback(string matchedValue);
}

