﻿namespace System
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [NonScriptable, EditorBrowsable(EditorBrowsableState.Never)]
    public struct UIntPtr
    {
    }
}

