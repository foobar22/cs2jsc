﻿namespace System
{
    using System.Runtime.CompilerServices;

    [Extended, IgnoreNamespace]
    public struct Void
    {
    }
}

