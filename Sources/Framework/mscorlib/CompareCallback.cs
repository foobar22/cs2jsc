﻿namespace System
{
    using System.Runtime.CompilerServices;

    [IgnoreNamespace, Extended]
    public delegate int CompareCallback(object x, object y);
}

