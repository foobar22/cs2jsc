﻿namespace System
{
    using System.Runtime.CompilerServices;

    [Extended, IgnoreNamespace]
    public delegate void EventHandler(object sender, EventArgs e);
}

