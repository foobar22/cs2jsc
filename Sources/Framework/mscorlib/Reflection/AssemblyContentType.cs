﻿//-----------------------------------------------------------------------
// <copyright file="AssemblyContentType.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Reflection
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

	[Serializable]
	public enum AssemblyContentType
	{
		Default,
		WindowsRuntime
	}
}
