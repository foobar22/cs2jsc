﻿namespace System
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [EditorBrowsable(EditorBrowsableState.Never), NonScriptable, Extended]
    public sealed class ParamArrayAttribute : Attribute
    {
    }
}

