﻿//-----------------------------------------------------------------------
// <copyright file="Guid.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System
{
    using System;

    /// <summary>
    /// Definition for Guid
    /// </summary>
    public struct Guid
    {
        private int f1;
        private short s1;
        private short s2;
        private byte b1;
        private byte b2;
        private byte b3;
        private byte b4;
        private byte b5;
        private byte b6;
        private byte b7;
        private byte b8;
    }
}
