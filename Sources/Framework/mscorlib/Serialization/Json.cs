﻿namespace System.Serialization
{
    using System;
    using System.Runtime.CompilerServices;

    [ScriptName("JSON"), Extended, IgnoreNamespace]
    public static class Json
    {
        public static object Parse(string json)
        {
            return null;
        }

        public static object Parse(string json, JsonParseCallback parseCallback)
        {
            return null;
        }

        public static string Stringify(object o)
        {
            return null;
        }

        public static string Stringify(object o, string[] serializableMembers)
        {
            return null;
        }

        public static string Stringify(object o, JsonStringifyCallback callback)
        {
            return null;
        }

        public static string Stringify(object o, string[] serializableMembers, int indentSpaces)
        {
            return null;
        }

        public static string Stringify(object o, string[] serializableMembers, string indentText)
        {
            return null;
        }

        public static string Stringify(object o, JsonStringifyCallback callback, int indentSpaces)
        {
            return null;
        }

        public static string Stringify(object o, JsonStringifyCallback callback, string indentText)
        {
            return null;
        }
    }
}

