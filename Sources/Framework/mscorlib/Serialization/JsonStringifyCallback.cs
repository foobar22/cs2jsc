﻿namespace System.Serialization
{
    using System;
    using System.Runtime.CompilerServices;

    [Extended, IgnoreNamespace]
    public delegate object JsonStringifyCallback(string name, object value);
}

