﻿namespace System
{
    using System.Runtime.CompilerServices;

    [Extended, IgnoreNamespace]
    public delegate void ContextualCallback(object context);
}

