﻿//-----------------------------------------------------------------------
// <copyright file="VisualFilter.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Web.Html.Filters
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Definition for VisualFilter
    /// </summary>
    [IgnoreNamespace]
    public class VisualFilter
    {
        internal extern VisualFilter();

        public extern bool Enabled { get; set; }
    }
}