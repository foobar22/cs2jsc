﻿namespace System.Web.Html
{
    public enum Orientation
    {
        Landscape,
        Portrait
    }
}