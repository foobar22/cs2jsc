﻿//-----------------------------------------------------------------------
// <copyright file="Constants.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Web.Html
{
    /// <summary>
    /// Definition for Constants
    /// </summary>
    public static class DataFormatStrings
    {
        public const string Text = "Text";
        public const string URL = "URL";
    }
}