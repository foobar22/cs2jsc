﻿//-----------------------------------------------------------------------
// <copyright file="AudioElement.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Web.Html
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Definition for AudioElement
    /// </summary>
    [IgnoreNamespace]
    public sealed class AudioElement : MediaElement
    {
        private extern AudioElement();
    }
}
