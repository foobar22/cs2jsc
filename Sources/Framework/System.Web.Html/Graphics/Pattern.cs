﻿//-----------------------------------------------------------------------
// <copyright file="Pattern.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Web.Html.Graphics
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Definition for Pattern
    /// </summary>
    [IgnoreNamespace]
    public class Pattern
    {
        private extern Pattern();
    }
}