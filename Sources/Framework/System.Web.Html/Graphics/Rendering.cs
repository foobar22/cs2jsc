﻿//-----------------------------------------------------------------------
// <copyright file="Rendering.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Web.Html.Graphics
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Definition for Rendering
    /// </summary>
    [IgnoreNamespace]
    public sealed class Rendering
    {
        private extern Rendering();
    }
}