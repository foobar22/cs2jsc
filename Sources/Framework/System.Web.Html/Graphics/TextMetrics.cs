﻿//-----------------------------------------------------------------------
// <copyright file="TextMetrics.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Web.Html.Graphics
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Definition for TextMetrics
    /// </summary>
    [IgnoreNamespace]
    public sealed class TextMetrics
    {
        private extern TextMetrics();

        public extern double Width { get; }
    }
}