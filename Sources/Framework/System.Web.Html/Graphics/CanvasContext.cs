﻿//-----------------------------------------------------------------------
// <copyright file="CanvasContext.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Web.Html.Graphics
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Definition for CanvasContext
    /// </summary>
    [IgnoreNamespace]
    public abstract class CanvasContext
    {
        internal extern CanvasContext();
    }
}