﻿//-----------------------------------------------------------------------
// <copyright file="GeolocationService.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Web.Html.Geolocation
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Definition for GeolocationService
    /// </summary>
    [IgnoreNamespace]
    public class GeolocationService
    {
        private extern GeolocationService();
    }
}