using System;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Metadata;
namespace Windows.Graphics.Printing
{
	[MarshalingBehavior(MarshalingType.Agile), Muse(Version = 100794368u), Version(100794368u)]
	public sealed class PrintTask : IPrintTask
	{
		public extern event TypedEventHandler<PrintTask, PrintTaskCompletedEventArgs> Completed;
		public extern event TypedEventHandler<PrintTask, object> Previewing;
		public extern event TypedEventHandler<PrintTask, PrintTaskProgressingEventArgs> Progressing;
		public extern event TypedEventHandler<PrintTask, object> Submitting;
		public extern PrintTaskOptions Options
		{
			get;
		}
		public extern DataPackagePropertySet Properties
		{
			get;
		}
		public extern IPrintDocumentSource Source
		{
			get;
		}
	}
}
