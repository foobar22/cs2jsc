using System;
using Windows.Foundation.Metadata;
namespace Windows.UI.Xaml.Automation.Peers
{
	[Version(100794368u), WebHostHidden]
	public enum AutomationLiveSetting
	{
		Off,
		Polite,
		Assertive
	}
}
