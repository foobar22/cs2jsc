using System;
using Windows.Foundation.Metadata;
namespace Windows.UI.ApplicationSettings
{
	[Version(100794368u)]
	public enum SettingsEdgeLocation
	{
		Right,
		Left
	}
}
