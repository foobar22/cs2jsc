using System;
using Windows.Foundation.Metadata;
namespace Windows.Foundation
{
	[Version(100794368u)]
	public struct Size
	{
		public float Width;
		public float Height;
	}
}
