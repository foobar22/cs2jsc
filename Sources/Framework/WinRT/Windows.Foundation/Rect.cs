using System;
using Windows.Foundation.Metadata;
namespace Windows.Foundation
{
	[Version(100794368u)]
	public struct Rect
	{
		public float X;
		public float Y;
		public float Width;
		public float Height;
	}
}
