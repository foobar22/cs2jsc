using System;
using Windows.Foundation.Metadata;
namespace Windows.Foundation
{
	[Version(100794368u)]
	public struct TimeSpan
	{
		public long Duration;
	}
}
