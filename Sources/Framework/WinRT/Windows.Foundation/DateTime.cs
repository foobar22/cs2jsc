using System;
using Windows.Foundation.Metadata;
namespace Windows.Foundation
{
	[Version(100794368u)]
	public struct DateTime
	{
		public long UniversalTime;
	}
}
