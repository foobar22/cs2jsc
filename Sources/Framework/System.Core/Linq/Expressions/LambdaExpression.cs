﻿//-----------------------------------------------------------------------
// <copyright file="LambdaExpression.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace System.Linq.Expressions
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.CompilerServices;

	/// <summary>Describes a lambda expression. This captures a block of code that is similar to a .NET method body.</summary>
	public abstract class LambdaExpression : Expression
	{
		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.LambdaExpression.Type" /> that represents the static type of the expression.</returns>
		public extern sealed override Type Type
		{
			get;
		}

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		public extern sealed override ExpressionType NodeType
		{
			get;
		}

		/// <summary>Gets the parameters of the lambda expression.</summary>
		/// <returns>A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of <see cref="T:System.Linq.Expressions.ParameterExpression" /> objects that represent the parameters of the lambda expression.</returns>
		public extern ReadOnlyCollection<ParameterExpression> Parameters
		{
			get;
		}

		/// <summary>Gets the name of the lambda expression.</summary>
		/// <returns>The name of the lambda expression.</returns>
		public extern string Name
		{
			get;
		}

		/// <summary>Gets the body of the lambda expression.</summary>
		/// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> that represents the body of the lambda expression.</returns>
		public extern Expression Body
		{
			get;
		}

		/// <summary>Gets the return type of the lambda expression.</summary>
		/// <returns>The <see cref="T:System.Type" /> object representing the type of the lambda expression.</returns>
		public extern Type ReturnType
		{
			get;
		}

		/// <summary>Gets the value that indicates if the lambda expression will be compiled with the tail call optimization.</summary>
		/// <returns>True if the lambda expression will be compiled with the tail call optimization, otherwise false.</returns>
		public extern bool TailCall
		{
			get;
		}

		/// <summary>Produces a delegate that represents the lambda expression.</summary>
		/// <returns>A <see cref="T:System.Delegate" /> that contains the compiled version of the lambda expression.</returns>
		public extern Delegate Compile();

		/// <summary>Produces a delegate that represents the lambda expression.</summary>
		/// <returns>A delegate containing the compiled version of the lambda.</returns>
		/// <param name="debugInfoGenerator">Debugging information generator used by the compiler to mark sequence points and annotate local variables.</param>
		//// public extern Delegate Compile(DebugInfoGenerator debugInfoGenerator);

		/// <summary>Compiles the lambda into a method definition.</summary>
		/// <param name="method">A <see cref="T:System.Reflection.Emit.MethodBuilder" /> which will be used to hold the lambda's IL.</param>
		//// public extern void CompileToMethod(MethodBuilder method);

		/// <summary>Compiles the lambda into a method definition and custom debug information.</summary>
		/// <param name="method">A <see cref="T:System.Reflection.Emit.MethodBuilder" /> which will be used to hold the lambda's IL.</param>
		/// <param name="debugInfoGenerator">Debugging information generator used by the compiler to mark sequence points and annotate local variables.</param>
		//// public extern void CompileToMethod(MethodBuilder method, DebugInfoGenerator debugInfoGenerator);
	}
}
