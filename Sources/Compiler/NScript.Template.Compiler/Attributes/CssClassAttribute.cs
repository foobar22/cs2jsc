﻿//-----------------------------------------------------------------------
// <copyright file="CssClassAttribute.cs" company="Microsoft Corp.">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace NScript.Template.Compiler.Attributes
{
    /// <summary>
    /// CssClass attribute reader.
    /// </summary>
    public class CssClassAttribute : AttributeBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CssClassAttribute"/> class.
        /// </summary>
        /// <param name="cssClassName">Name of the CSS class.</param>
        public CssClassAttribute()
        {
        }
    }
}
