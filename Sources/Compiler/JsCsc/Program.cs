﻿using NScript.Csc.Lib;

namespace JsCsc;

class Program
{
    static int Main(string[] args)
    {
        return CscCompiler.Main(args);
    }
}
