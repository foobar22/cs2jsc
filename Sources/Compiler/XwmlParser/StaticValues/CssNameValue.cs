﻿//-----------------------------------------------------------------------
// <copyright file="CssNameValue.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace XwmlParser.StaticValues
{
    using NScript.JST;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Definition for CssNameValue
    /// </summary>
    public class CssNameValue : StaticValue
    {
        private IIdentifier[] cssClassNames;

        public CssNameValue(
            DocumentContext documentContext,
            string cssClassName)
        {
            var cssClassNames = cssClassName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            this.cssClassNames = new IIdentifier[cssClassNames.Length];
            for (int iCssClassName = 0; iCssClassName < cssClassNames.Length; iCssClassName++)
            {
                if (string.IsNullOrWhiteSpace(cssClassNames[iCssClassName]))
                { continue; }

                cssClassName = cssClassNames[iCssClassName];
                IIdentifier classIdentifier;
                if (!documentContext.TryGetCssClassIdentifier(cssClassName, out classIdentifier))
                {
                    throw new ApplicationException(
                        string.Format("CSS class name '{0}' not found.", cssClassName));
                }

                this.cssClassNames[iCssClassName] = classIdentifier;
            }
        }

        /// <summary>
        /// Gets initialization expression.
        /// </summary>
        /// <param name="codeGenerator"> The code generator. </param>
        /// <returns>
        /// The initialization expression.
        /// </returns>
        public override Expression GetInitializationExpression(
            SkinCodeGenerator codeGenerator)
        {
            IdentifierStringExpression rv = null;
            for (int iClass = 0; iClass < this.cssClassNames.Length; iClass++)
            {
                if (iClass == 0)
                {
                    rv = new IdentifierStringExpression(
                            null,
                            codeGenerator.Scope,
                            new IdentifierExpression(
                                this.cssClassNames[iClass],
                                codeGenerator.Scope));
                }
                else
                {
                    rv.Append(new StringLiteralExpression(codeGenerator.Scope, " "));
                    rv.Append(
                        new IdentifierExpression(
                                this.cssClassNames[iClass],
                                codeGenerator.Scope));
                }
            }

            if (rv == null)
            {
                return new StringLiteralExpression(
                    codeGenerator.Scope,
                    string.Empty);
            }

            return rv;
        }
    }
}
