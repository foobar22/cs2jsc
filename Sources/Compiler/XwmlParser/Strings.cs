﻿//-----------------------------------------------------------------------
// <copyright file="Strings.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace XwmlParser
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Definition for Strings
    /// </summary>
    public static class Strings
    {
        public const string Skin = "skin";
        public const string Template = "template";
        public const string CssStyleTag = "style";
        public const string LinkTag = "link";
        public const string Meta = "meta";
        public const string Title = "title";
        public const string Body = "body";
    }
}
