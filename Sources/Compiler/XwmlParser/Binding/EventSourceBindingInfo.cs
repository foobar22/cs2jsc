﻿//-----------------------------------------------------------------------
// <copyright file="EventSourceBindingInfo.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace XwmlParser.Binding
{
    using Mono.Cecil;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Definition for EventSourceBindingInfo
    /// </summary>
    public class EventSourceBindingInfo : PropertySourceBindingInfo
    {
        public EventSourceBindingInfo()
            : base(null, null)
        { }
    }
}
