- ~~Readonly members~~ (Free)
- Default interface methods
- Pattern matching enhancements:
    - Switch expressions
    - Property patterns
    - Tuple patterns
    - Positional patterns
- Using declarations
- ~~Static local functions~~ (Free)
- ~~Disposable ref structs~~ (Irrelevant to JS target)
- Nullable reference types
- Asynchronous streams
- Indices and ranges
- ~~Null-coalescing assignment~~
- ~~Unmanaged constructed types~~ (Irrelevant to JS target)
- ~~Stackalloc in nested expressions~~ (Irrelevant to JS target)
- ~~Enhancement of interpolated verbatim strings~~ (Free)

Bug fixes:
1. Generic get only properties doesn't work
2. Fix throw expressions code generation