//-----------------------------------------------------------------------
// <copyright file="$fileinputname$.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace $rootnamespace$
{
    using System;
    using SunlightUnit;

    /// <summary>
    /// Definition for $safeitemrootname$
    /// </summary>
    [TestFixture]
    public class $safeitemrootname$
    {
        [TestSetup]
        /// <summary>
        /// Sets up the data/environment to run all the test cases.
        /// </summary>
        public static void Setup()
        {
        }

        [Test]
        /// <summary>
        /// Test case to test a unit of functionality.
        /// </summary>
        public static void Test()
        {
            Assert.IsTrue(true, "true should be true");
        }
    }
}

